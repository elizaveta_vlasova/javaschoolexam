package com.tsystems.javaschool.tasks.calculator;

import java.util.Collections;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {
    private final String OPERATORS = "+*/-";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            checkInput(statement);
            Stack<String> stackRPN = toRPN(statement);
            String result = evaluateFromRPN(stackRPN);
            return result;
        } catch (IllegalArgumentException a) {
            return null;
        }
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    private boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (NumberFormatException e) {
            if (token.indexOf(",") >= 0 || token.indexOf(".") >= 0) {
                throw new IllegalArgumentException(e);
            }
            return false;
        }
        return true;
    }

    private void checkInput(String statement) {
        if (statement == null || statement.equals("")) throw new IllegalArgumentException("");
        if (statement.indexOf("++") > 0 || statement.indexOf("--") > 0 ||
                statement.indexOf("**") > 0 || statement.indexOf("//") > 0) throw new IllegalArgumentException("");
        int countOpenBraces = statement.length() - statement.replace("(", "").length();
        int countCloseBraces = statement.length() - statement.replace(")", "").length();
        if (countOpenBraces != countCloseBraces) throw new IllegalArgumentException("");
    }

    private Stack<String> toRPN(String statement) {
        Stack<String> stackRPN = new Stack<>();
        StringTokenizer stringTokenizer = new StringTokenizer(statement, OPERATORS + "()", true);
        Stack operators = new Stack();

        while (stringTokenizer.hasMoreElements()) {
            String token = stringTokenizer.nextToken();
            if (isOpenBracket(token)) {
                operators.push(token);
            } else if (isCloseBracket(token)) {
                if (!operators.contains("(")) throw new IllegalArgumentException("");
                while (!operators.empty() && !isOpenBracket((String) operators.lastElement())) {
                    stackRPN.push((String) operators.pop());
                }
                operators.pop();
                if (!operators.empty()) {
                    stackRPN.push((String) operators.pop());
                }
            } else if (isNumber(token)) {
                stackRPN.push(token);
            } else if (isOperator(token)) {
                while (!operators.empty() && isOperator((String) operators.lastElement())
                        && getPrecedence(token) <= getPrecedence((String) operators.lastElement())) {
                    stackRPN.push((String) operators.pop());
                }
                operators.push(token);
            }
        }
        while (!operators.empty()) {
            stackRPN.push((String) operators.pop());
        }
        return stackRPN;
    }

    private String evaluateFromRPN(Stack<String> stackRPN) {
        Collections.reverse(stackRPN);

        Stack<String> answer = new Stack<>();

        while (!stackRPN.isEmpty()) {
            String token = stackRPN.pop();
            if (isNumber(token)) {
                answer.push(token);
            } else if (isOperator(token)) {
                String result = evaluate(answer.pop(), answer.pop(), token);
                answer.push(result);
            }
        }
        return answer.pop();
    }

    private String evaluate(String a, String b, String operation) {
        Double first = Double.parseDouble(a);
        Double second = Double.parseDouble(b);
        Double result = 0.0;
        switch (operation) {
            case "+": {
                result = (first + second);
                break;
            }
            case "-": {
                result = (second - first);
                break;
            }
            case "*": {
                result = (first * second);
                break;
            }
            case "/": {
                result = (second / first);
                break;
            }
        }
        if (result.isInfinite()) {
            return null;
        }
        Integer intValue = result.intValue();
        double fPart = result - intValue;
        if (fPart == 0) {
            return intValue.toString();
        }
        return result.toString();
    }

    private byte getPrecedence(String token) {
        if (token.equals("+") || token.equals("-")) {
            return 1;
        }
        return 2;
    }
}