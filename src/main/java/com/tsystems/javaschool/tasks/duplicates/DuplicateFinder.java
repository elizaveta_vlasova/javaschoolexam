package com.tsystems.javaschool.tasks.duplicates;

import java.io.File;

import java.io.*;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;


public class DuplicateFinder {
    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        try {
            TreeMap<String, Integer> map = new TreeMap();
            BufferedReader reader = new BufferedReader(new FileReader("a.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                Integer previousValue = map.put(line, 1);
                if (previousValue != null) {
                    try {
                        map.put(line, previousValue + 1);
                    } catch (NoSuchElementException e) {
                    }
                }
            }

            FileWriter fw = new FileWriter(targetFile, true);
            Iterator it = map.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                fw.write(pair.getKey() + "[" + pair.getValue() + "]" + "\r");
                it.remove(); // avoids a ConcurrentModificationException
            }
            fw.close();

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
