# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The solution is to be written using Java 1.8 only, external libraries are forbidden. 
You can add dependencies with scope "test" if it's needed to write new unit-tests.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Subsequence](/tasks/Subsequence.md), [Duplicates](/tasks/Duplicates.md)

### Result ###

* Author name : Vlasova_Elizaveta
* Codeship : [ ![Codeship Status for elizaveta_vlasova/javaschoolexam](https://app.codeship.com/projects/7e8f0600-1f98-0135-c3d3-165b0e29b86b/status?branch=master)](https://app.codeship.com/projects/221008)


